=== InstaFeed ===
Contributors: lukoie
Donate link: http://www.gnu.org/
Tags: instagram, feed, pictures, photos
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The InstaFeed Wordpress Plugin is intended for showig the Instagram feed on your Wordpress site.

== Description ==

The InstaFeed Wordpress Plugin is intended for showig the Instagram feed on your Wordpress site.

== Installation ==

Use standart Wordpress functionality for adding plugins, or

0. Upload plugin folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about this? =

Yes, we can do that.

== Screenshots ==

1. The first screenshot
2. The second screenshot
3. The third screenshot

== Changelog ==

= 1.0 =
* Initial release